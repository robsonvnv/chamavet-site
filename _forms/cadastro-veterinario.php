<?php
	
	$nonce	=	wp_create_nonce("add_veterinario_nonce");
	// $link	=	admin_url('admin-ajax.php?action=add_veterinario&nonce=' . $nonce);
	$link	=	admin_url('admin-ajax.php?nonce=' . $nonce);

?>
<style type="text/css">

	#cadastro-de-veterinarios-result {
		
		text-align:			center;
		display:			none;
		color:				#666666;

	}

	#cadastro-de-veterinarios-result #box {

		border-radius:		100%;
		font-size:			30px;
		position:			relative;
		display:			table;
		padding:			20px 25px;
		border:				4px solid #666666;
		margin:				20px auto;

	}


	#cadastro-de-veterinarios-result #mensagem { font-size: 22px; padding-bottom: 20px; }




	#cadastro-de-veterinarios-loading { font-size: 22px; display: none; }
	#cadastro-de-veterinarios-loading .glyphicon { font-size: 40px; }


	.glyphicon-refresh-animate {

		-animation:			spin .7s infinite linear;
		-webkit-animation:	spin2 .7s infinite linear;

	}


	@-webkit-keyframes spin2 {

		from { -webkit-transform: rotate(0deg); }
		to { -webkit-transform: rotate(360deg); }

	}

	@keyframes spin {
		from { transform: scale(1) rotate(0deg); }
		to { transform: scale(1) rotate(360deg); }
	}

</style>
<script type="text/javascript">

	$(function() {

		$("#add-veterinario-form").submit(function() {

			var link	=	$(this).attr('action');

			var campos	=	{
				
				nome: {

					campo: 'nome',
					mensagem: 'Por favor preencha o campo Nome.'
				},
				email: {

					campo: 'email',
					mensagem: 'Por favor preencha o campo Email.'

				},
				crmv: {

					campo: 'crmv',
					mensagem: 'Por favor preencha o campo CRMV.'

				},
				rg: {

					campo: 'rg',
					mensagem: 'Por favor preencha o campo RG.'

				},
				cpf: {

					campo: 'cpf',
					mensagem: 'Por favor preencha o campo CPF.'

				},
				fixo: {

					campo: 'fixo',
					mensagem: 'Por favor preencha o campo Telefone fixo.'

				},
				celular: {

					campo: 'celular',
					mensagem: 'Por favor preencha o campo Celular.'

				},
				atuacao: {

					campo: 'estado-atuacao',
					mensagem: 'Por favor selecione seu Estado de atuação.'

				},
				graduacao: {

					campo: 'universidade-graduacao',
					mensagem: 'Por favor preencha o campo Universidade de Graduação.'

				},
				ano: {

					campo: 'ano-graduacao',
					mensagem: 'Por favor preencha o campo Ano de Graduação.'

				},
				experiencia: {

					campo: 'experiencia-profissional',
					mensagem: 'Por favor preencha o campo Experiência Profissional.'

				},
				residencialCep: {

					campo: 'residencial-cep',
					mensagem: 'Por favor preencha o campo Cep do seu Endereço Residencial.'

				},
				residencialCidade: {

					campo: 'residencial-cidade',
					mensagem: 'Por favor preencha o campo Cidade do seu Endereço Residencial.'

				},
				residencialBairro: {

					campo: 'residencial-bairro',
					mensagem: 'Por favor preencha o campo Bairro do seu Endereço Residencial.'

				},
				residencialEndereco: {

					campo: 'residencial-endereco',
					mensagem: 'Por favor preencha o campo Endereço do seu Endereço Residencial.'

				},
				residencialNumero: {

					campo: 'residencial-numero',
					mensagem: 'Por favor preencha o campo Numero do seu Endereço Residencial.'

				},
				comercialCep: {

					campo: 'comercial-cep',
					mensagem: 'Por favor preencha o campo Cep do seu Endereço Comercial.'

				},
				comercialCidade: {

					campo: 'comercial-cidade',
					mensagem: 'Por favor preencha o campo Cidade do seu Endereço Comercial.'

				},
				comercialBairro: {

					campo: 'comercial-bairro',
					mensagem: 'Por favor preencha o campo Bairro do seu Endereço Comercial.'

				},
				comercialEndereco: {

					campo: 'comercial-endereco',
					mensagem: 'Por favor preencha o campo Endereço do seu Endereço Comercial.'

				},
				comerciallNumero: {

					campo: 'comercial-numero',
					mensagem: 'Por favor preencha o campo Numero do seu Endereço Comercial.'

				}

			};

			var continuar	=	true;
			var mensagem	=	'';
			var valores		=	[];

			$.each(campos, function(index, value) {

				if (continuar == true) {

					var valor	=	$('#' + value.campo).val();
					if (!valor && (valor == '' || valor == null)) {

						mensagem	=	value.mensagem;
						continuar	=	false;

					} else {

						valores.push( { "campo": index, "value": valor } );

					}

				}

			});

			if (continuar == false) {

				alert(mensagem);

			} else {

				$("#cadastro-de-veterinarios-content").fadeOut(500, function() {

					$("#cadastro-de-veterinarios-loading").fadeIn(500, function() {

						$.ajax({

							url:		link,
							type:		'POST',
							dataType:	"json",
							data:		{ action: 'add_veterinario', valores: valores },
							success:	function(response) {

								$("#cadastro-de-veterinarios-loading").fadeOut(500, function() {

									$('#cadastro-de-veterinarios-result #mensagem').html(response.mensagem);
									$('#cadastro-de-veterinarios-result #box > #box-icon').removeAttr('class');
									$('#cadastro-de-veterinarios-result #box > #box-icon').addClass('glyphicon');

									if(response.result == true) {

										$('#cadastro-de-veterinarios-result').css('color', 'green');
										$('#cadastro-de-veterinarios-result #box').css('border-color', 'green');
										$('#cadastro-de-veterinarios-result #box > #box-icon').addClass('glyphicon-ok');
										setTimeout(function() {

											$("#cadastro-de-veterinarios").modal('hide');
											$("#cadastro-de-veterinarios").on('hidden.bs.modal', function() {

												$("#add-veterinario-form")[0].reset();
												$('#cadastro-de-veterinarios-result').css('display', 'none');
												$('#cadastro-de-veterinarios-content').css('display', 'block');
												
											});

										},
										5000);

									} else {

										$('#cadastro-de-veterinarios-result').css('color', 'red');
										$('#cadastro-de-veterinarios-result #box').css('border-color', 'red');
										$('#cadastro-de-veterinarios-result #box > #box-icon').addClass('glyphicon-remove');
										$("#cadastro-de-veterinarios").on('hidden.bs.modal', function() {

											$('#cadastro-de-veterinarios-result').css('display', 'none');
											$('#cadastro-de-veterinarios-content').css('display', 'block');

										});
										setTimeout(function() {

											$("#cadastro-de-veterinarios-result").fadeOut(500, function() {

												$("#cadastro-de-veterinarios-content").fadeIn(500);

											});

										}, 5000);

									}

									$('#cadastro-de-veterinarios-result').fadeIn(500);


								});

							}

						});

					});

				});

			}

			return false;

		});

	});

</script>
<form id="add-veterinario-form" method="POST" action="<?php echo $link; ?>">

	<div id="cadastro-de-veterinarios" class="modal fade" tabindex="-1" role="dialog">
		
		<div class="modal-dialog modal-lg" role="document">
			
			<div class="modal-content">
				
				<div class="modal-header">
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title text-center">Forumário para cadastro de Veterinários</h4>
				
				</div>
				<div id="cadastro-de-veterinarios-result">
					
					<div id="box">

						<span id="box-icon" class="glyphicon glyphicon-asterisk"></span>

					</div>
					<br />
					<div id="mensagem"></div>

				</div>
				<div id="cadastro-de-veterinarios-loading">
					
					<div class="modal-body">

						<div class="text-center">

							<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>
							<br />
							Carregando ...

						</div>

					</div>

				</div>
				<div id="cadastro-de-veterinarios-content">
					<div class="modal-body">
					
						<div>

							<ul class="nav nav-pills" role="tablist">
								
								<li role="presentation" class="active"><a href="#pessoais" aria-controls="pessoais" role="tab" data-toggle="tab">Dados Pessoais</a></li>
								<li role="presentation"><a href="#profissional" aria-controls="profissional" role="tab" data-toggle="tab">Curriculo Profissional</a></li>
								<li role="presentation"><a href="#residencial" aria-controls="residencial" role="tab" data-toggle="tab">Endereço Residencial</a></li>
								<li role="presentation"><a href="#comercial" aria-controls="comercial" role="tab" data-toggle="tab">Endereço Comercial</a></li>

							</ul>
							<br />
							<br />
							<div class="tab-content">
								
								<div role="tabpanel" class="tab-pane active" id="pessoais">

									<div class="row">

										<div class="col-xs-12">
											
											<label for="nome">Nome Completo <span>*</span></label>
											<br />
											<input type="text" name="nome" id="nome" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12">
											
											<label for="email">Email <span>*</span></label>
											<br />
											<input type="email" name="email" id="email" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-6">
											
											<label for="crmv">CRMV <span>*</span></label>
											<br />
											<input type="text" name="crmv" id="crmv" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-6">
											
											<label for="rg">RG <span>*</span></label>
											<br />
											<input type="text" name="rg" id="rg" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-6">
											
											<label for="cpf">CPF <span>*</span></label>
											<br />
											<input type="text" name="cpf" id="cpf" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-6">
											
											<label for="fixo">Telefone fixo <span>*</span></label>
											<br />
											<input type="text" name="fixo" id="fixo" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-6">
											
											<label for="celular">Celular <span>*</span></label>
											<br />
											<input type="text" name="celular" id="celular" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-4">
											
											<label for="estado-atuacao">Estado de atuação <span>*</span></label>
											<br />
											<input type="text" name="estado-atuacao" id="estado-atuacao" class="form-control" />

										</div>

									</div>

								</div>
								<div role="tabpanel" class="tab-pane" id="profissional">

									<div class="row">

										<div class="col-xs-12 col-sm-7">
											
											<label for="universidade-graduacao">Universidade de graduação</label>
											<br />
											<input type="text" name="universidade-graduacao" id="universidade-graduacao" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-5">
											
											<label for="ano-graduacao">Ano de conclusão</label>
											<br />
											<input type="text" name="ano-graduacao" id="ano-graduacao" class="form-control" />

										</div>
										<div class="col-xs-6"></div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12">

											<label for="experiencia-profissional">Experiência Profissional</label>
											<br />
											<textarea name="experiencia-profissional" id="experiencia-profissional" class="form-control" rows="3"></textarea>

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12"><hr /></div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12">(residência, pós graduação, estágios, titulações e outras formações complementares)</div>
									
									</div>
									<br />
									<div class="row">

										<div class="col-xs-6">
											
											<label for="profissional-titulo-1">Titulo <span>*</span></label>
											<br />
											<input type="text" name="profissional-titulo[]" id="profissional-titulo-1" class="form-control" />

										</div>
										<div class="col-xs-6">
											
											<label for="profissional-instituicao-1">Instituição <span>*</span></label>
											<br />
											<input type="text" name="profissional-instituicao[]" id="profissional-instituicao-1" class="form-control" />

										</div>
										<div class="col-xs-6"></div>

									</div>
									<br />
									<div class="row">
										<div class="col-xs-12"><button class="btn btn-primary center-block">Adicionar titulo</button></div>
									</div>

								</div>
								<div role="tabpanel" class="tab-pane" id="residencial">

									<div class="row">

										<div class="col-xs-12 col-sm-4">
											
											<label for="residencial-cep">Cep <span>*</span></label>
											<br />
											<input type="text" name="residencial-cep" id="residencial-cep" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-6">
											
											<label for="residencial-cidade">Cidade <span>*</span></label>
											<br />
											<input type="text" name="residencial-cidade" id="residencial-cidade" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-6">
											
											<label for="residencial-bairro">Bairro <span>*</span></label>
											<br />
											<input type="text" name="residencial-bairro" id="residencial-bairro" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-8">
											
											<label for="residencial-endereco">Endereço <span>*</span></label>
											<br />
											<input type="text" name="residencial-endereco" id="residencial-endereco" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-4">
											
											<label for="residencial-numero">N. <span>*</span></label>
											<br />
											<input type="text" name="residencial-numero" id="residencial-numero" class="form-control" />

										</div>

									</div>

								</div>
								<div role="tabpanel" class="tab-pane" id="comercial">

									<div class="row">

										<div class="col-xs-12 col-sm-4">
											
											<label for="comercial-cep">Cep <span>*</span></label>
											<br />
											<input type="text" name="comercial-cep" id="comercial-cep" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-6">
											
											<label for="comercial-cidade">Cidade <span>*</span></label>
											<br />
											<input type="text" name="comercial-cidade" id="comercial-cidade" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-6">
											
											<label for="comercial-bairro">Bairro <span>*</span></label>
											<br />
											<input type="text" name="comercial-bairro" id="comercial-bairro" class="form-control" />

										</div>

									</div>
									<br />
									<div class="row">

										<div class="col-xs-12 col-sm-8">
											
											<label for="comercial-endereco">Endereço <span>*</span></label>
											<br />
											<input type="text" name="comercial-endereco" id="comercial-endereco" class="form-control" />

										</div>
										<div class="col-xs-12 col-sm-4">
											
											<label for="residencial-numero">N. <span>*</span></label>
											<br />
											<input type="text" name="comercial-numero" id="comercial-numero" class="form-control" />

										</div>

									</div>

								</div>
							
							</div>

						</div>
				
					</div>
					<div class="modal-footer">
						
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-success">Enviar</button>
						<!-- <button type="button" class="btn btn-success" disabled>Enviar</button> -->
					
					</div>

				</div>
			
			</div>

		</div>

	</div>

</form>