<?php
	
	get_header();

		the_post();
		echo '<div class="container">';

			echo '<div class="row">';

				echo '<div class="col-xs-12">';

					echo '<h3>';
						
						wp_title();

					echo '</h3>';
					echo the_date();
					echo ' - Por ' . get_author_name();

				echo '</div>';

			echo '</div>';
			echo '<br />';
			echo '<div class="row">';

				echo '<div class="col-xs-12 col-sm-9">';

					the_content();

				echo '</div>';
				echo '<div class="col-xs-12 col-sm-3">';

					get_sidebar();
					
				echo '</div>';

			echo '</div>';

		
		echo '</div>';

	get_footer();

?>