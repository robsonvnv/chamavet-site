$(function() {

	$("#header-menu > li > a").click(function() {

		var home	=	$(this).hasClass('in-home');

		if (home == true) {

			var conteudo	=	$(this).attr('data-content');
			var total		=	conteudo.length;
			var topo		=	$('#' + conteudo).offset().top;
			$('html, body').animate({ scrollTop: topo }, 500);
			return false;

		}

	});


	$("header #open").click(function() {

		$('#header-nav').animate({ left: '0px' }, 500);

	});


	$("header #close").click(function() {

		$('#header-nav').animate({ left: '-100%' }, 500);

	});


	$(window).scroll(function() {

		var menu	=	$('#header-nav').css('left');
		if (menu == '0px') {

			$('#header-nav').animate({ left: '-100%' }, 500);

		}

	});

});