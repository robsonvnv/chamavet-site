<?php
	
	if (is_front_page()) {

		require_once( __DIR__ . '/index.php' );

	} else {

		get_header();

			the_post();
			echo '<div class="container">';
			
				echo '<div class="row">';

					echo '<div class="col-xs-12">';

						echo '<h2>';
							
							wp_title();

						echo '</h2>';

					echo '</div>';

				echo '</div>';
				echo '<div class="row">';

					echo '<div class="col-xs-12">';

						the_content();

					echo '</div>';

				echo '</div>';

			echo '</div>';

		get_footer();

	}

?>