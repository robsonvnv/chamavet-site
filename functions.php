<?php
	
	function theme_add_veterinario() {

		$valores	 =	$_POST['valores'];
		if ($valores) {

			$total		 =	count($valores);
			$count		 = 1;
			$data		 =	"{\n";

				foreach ($valores as $valor) {

					if ($valor['campo'] != 'nome') {

						$data	.=	'"' . $valor['campo'] . '": "' . $valor['value'] . '"';
						if ($count < $total) { $data .= ", \n"; }

					} else {

						$nome	 =	$valor['value'];

					}

					$count++;
					
				}

			$data		.=	"\n}";

			$insert		 =	wp_insert_post(

				array(

					'post_type'			=>	'veterinarios',
					'post_title'		=>	$nome,
					'post_content'		=>	$data,
					'post_status'		=>	'publish',
					'comment_status'	=>	'closed',
					'ping_status'		=>	'closed'

				)

			);
			
			if($insert != '') {

				$retorno['result']		=	true;
				$retorno['mensagem']	=	'Solicitação enviada com sucesso!';

			} else {

				$retorno['result']		=	false;
				$retorno['mensagem']	=	'Sua solicitação não pode ser enviada! Por favor tente novamente mais tarde ou entre em contato conosco para maiores informações!';

			}

			echo json_encode($retorno);

		}
		wp_die();

	}


	function chamavet_veterinarios() {

		require_once( __DIR__ . '/_forms/cadastro-veterinario.php');

	}

	function theme_register_shortcodes() {

		add_shortcode('cadastro-de-veterinario', 'chamavet_veterinarios');

	}


	// Registros dos post types do theme.
	function theme_register_post_types() {

		register_post_type (
			'home',
			array (
				
				'public'			=>	false,
				'publicy_queryable'	=>	true,
				'capability_type'	=>	'post',
				'query_var'			=>	true,
				'hierarchical'		=>	false,
				'show_ui'			=>	true,
				'has_archive'		=>	false,
				'menu_position'		=>	5,
				'menu_icon'			=>	'dashicons-admin-home',
				'with_front'		=>	true,
				'rewrite'			=>	array('slug' => 'home'),
				'supports'			=>	array('title', 'editor', 'thumbnail', 'page-attributes'),
				'labels'			=>	array(

					'name'					=>	'Home',
					'singular_name'			=>	'Item',
					'menu_name'				=>	'Home',
					'all_items'				=>	'Items',
					'add_new'				=>	'Adicionar Novo',
					'add_new_item'			=>	'Adicionar Novo Item',
					'edit'					=>	'Editar',
					'edit_item'				=>	'Editar Item',
					'new_item'				=>	'Novo Item',
					'view'					=>	'Ver Item',
					'view_item'				=>	'Ver Item',
					'search_items'			=>	'Pesquisar Item',
					'not_found'				=>	'Nenhum item encontrado.',
					'not_found_in_trash'	=>	'Nenhum item encontrado na lixeira.'

				)

			)

		);


		register_post_type (
			'veterinarios',
			array (
				
				'public'			=>	true,
				'publicy_queryable'	=>	true,
				'exclude_from_search'	=>	true,
				'capability_type'	=>	'post',
				'capabilities'		=>	array('create_posts' =>	false ),
				'query_var'			=>	true,
				'hierarchical'		=>	false,
				'show_ui'			=>	true,
				'has_archive'		=>	false,
				'menu_position'		=>	6,
				'menu_icon'			=>	'dashicons-admin-home',
				'rewrite'			=>	array(
					
					'with_front'		=>	false,
					'slug' => 'veterinarios'
					
				),
				'supports'			=>	array('title', 'editor'),
				'map_meta_cap'		=> 	true,
				'labels'			=>	array(

					'name'					=>	'Veterinários',
					'singular_name'			=>	'Solicitação',
					'menu_name'				=>	'Veterinários',
					'all_items'				=>	'Solicitações',
					'add_new'				=>	'Adicionar Solicitação',
					'add_new_item'			=>	'Adicionar Nova Solicitação',
					'edit'					=>	'Editar',
					'edit_item'				=>	'Editar Solicitação',
					'new_item'				=>	'Nova Solicitação',
					'view'					=>	'Ver Solicitação',
					'view_item'				=>	'Ver Solicitação',
					'search_items'			=>	'Pesquisar Solicitação',
					'not_found'				=>	'Nenhuma solicitação encontrado.',
					'not_found_in_trash'	=>	'Nenhuma solicitação encontrado na lixeira.'

				)

			)

		);

	}
	

	// Registro das sidebars do theme.
	function theme_register_sidebars() {

		register_sidebar(
			
			array(
				
				'id'			=>	'blog-sidebar',
				'name'			=>	__( 'Sidebar', 'textdomain' ),
				'description'	=>	__( 'Sidebar do blog', 'textdomain' ),
				'before_widget'	=>	'<aside id="%1$s" class="widget %2$s">',
				'after_widget'	=>	'</aside>',
				'before_title'	=>	'<h3 class="widget-title">',
				'after_title'	=>	'</h3>'

			)
		
		);

	}
	

	// Registro dos menus do theme.
	function theme_register_menus() {

		register_nav_menu('theme_header_menu', 'Menu do cabeçalho.');
		register_nav_menu('theme_footer_menu', 'Menu do rodapé.');

	}


	// Inicio das funções do theme,
	function theme_init() {

		add_theme_support('post-thumbnails');

		show_admin_bar(false);

		theme_register_shortcodes();
		theme_register_post_types();
		theme_register_menus();
		theme_register_sidebars();
		add_action('wp_ajax_add_veterinario', 'theme_add_veterinario');


	}

	
	add_action('after_setup_theme', 'theme_init');

?>