<?php
	
	get_header();
		
		echo '<div class="container">';

			echo '<div class="row">';

				echo '<div class="col-xs-12">';

					echo '<h3>';
						
						wp_title();

					echo '</h3>';

				echo '</div>';

			echo '</div>';
			echo '<br />';
			echo '<div class="row">';

				echo '<div class="col-xs-12 col-sm-9">';

					if (have_posts()):

						while (have_posts()):

							the_post();
							echo '<div class="row">';

								echo '<div class="col-xs-12">';

									echo '<h4>' . get_the_title() . '</h4>';
									the_date();
									echo '<br />';
									echo get_the_excerpt();
									echo '<br />';
									echo '<br />';
									echo '<a href="' . get_permalink() . '" class="btn btn-primary text-uppercase">Leia mais</a>';

								echo '</div>';

							echo '</div>';
							echo '<br />';
							echo '<br />';
							echo '<br />';

						endwhile;

					endif;

				echo '</div>';
				echo '<div class="col-xs-12 col-sm-3">';

					get_sidebar();

				echo '</div>';

			echo '</div>';

		echo '</div>';

	get_footer();

?>