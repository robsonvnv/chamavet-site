<?php
	
	// Alteraçãodsadsa
	get_header();

		echo '<link rel="stylesheet" type="text/css" media="all" href="' . get_bloginfo('template_url') . '/_assets/css/home.css" />';
		$args = array(
			
			'post_type'	=>	'home',
			'orderby'	=>	'menu_order',
			'order'		=>	'ASC'

		);

		$query		=	new WP_Query($args);
		$itens		=	$query->posts;
		$total		=	count($itens);
		if ($total >= 1) {

			foreach ($itens as $item) {
				
				$thumbnail	=	get_the_post_thumbnail_url($item->ID);
				echo '<section class="section" id="' . $item->post_name . '" style="background-image: url(\'' . $thumbnail . '\')">';

					echo '<div class="container">';

						echo apply_filters('the_content', $item->post_content);

					echo '</div>';

				echo '</section>';

			}

		} else {

			// Redirecionar para o blog ?

		}

	get_footer();

?>