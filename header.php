<!DOCTYPE html>
<html lang="pt-br">
	
	<head>

		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<title><?php bloginfo('name'); ?></title>

		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/_assets/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/_assets/css/responsive.css" />
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
		<?php wp_head(); ?>

		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/_assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/_assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/_assets/js/functions.js"></script>

	</head>
	<body>

		<div id="tudo">

			<?php

				if (($locations = get_nav_menu_locations()) && isset( $locations['theme_header_menu'] ) ) {

					$menu	=	wp_get_nav_menu_object($locations['theme_header_menu'] );
					$items	=	wp_get_nav_menu_items($menu->term_id);
					$total	=	count($items);

					if ($total >= 1) {

						echo '<header>';

							echo '<div id="header-button" class="container">';

								echo '<div class="row">';

									echo '<div class="col-xs-6">';

										echo '<div id="open" class="button">';

											echo '<span class="glyphicon glyphicon-align-justify"></span>';

										echo '</div>';

									echo '</div>';

								echo '</div>';

							echo '</div>';
							echo '<div id="header-nav" class="container">';

								echo '<div class="row">';

									echo '<div class="col-xs-6"><span class="glyphicon glyphicon-align-justify"></span></div>';
									echo '<div class="col-xs-6 text-right">';

										echo '<div id="close" class="button">';

											echo '<span class="glyphicon glyphicon-remove"></span>';

										echo '</div>';

									echo '</div>';

								echo '</div>';
								echo '<div class="row">';

									echo '<div class="col-xs-12">';

										echo '<ul id="header-menu">';

											foreach ($items as $item) {

												echo '<li>';

													if ($item->object == 'home') {
														
														$url	=	str_replace(get_bloginfo('url') . '/blog/home/', '', $item->url);
														$url	=	str_replace('/', '', $url);
														echo '<a href="' . get_bloginfo('url') . '#'. $url . '" data-content="' . $url . '" class="in-home">';

													} else {
														
														echo '<a href="' . $item->url . '">';

													}

														echo $item->title;

													echo '</a>';

												echo '</li>';

											}

										echo '</ul>';

									echo '</div>';

								echo '</div>';
								echo '<div class="row">';

									echo '<div class="col-xs-12"><a href="" class="btn btn-primary">Curtir nossa página no Facebook</a></div>';

								echo '</div>';
								echo '<div class="row">';

									echo '<div class="col-xs-12"><h1>' . get_bloginfo('name') . '</h1> - <h2>' . get_bloginfo('description') . '</h2></div>';

								echo '</div>';

							echo '</div>';

						echo '</header>';

					}

				}

			?>
			
			<div id="content">

				<?php

					if (!is_front_page()) {

						echo '<div id="header">';
							
							echo '<div class="container">';

								echo '<div class="row">';

									echo '<div class="col-xs-12">';

										echo '<a href="' . get_bloginfo('url') . '">';
											
											echo '<img src="' . get_bloginfo('template_url') . '/_assets/images/logo.png" class="img-responsive" />';
											echo '<div>ChamaVet</div>';

										echo '</a>';

									echo '</div>';

								echo '</div>';

							echo '</div>';

						echo '</div>';
						echo '<br />';
						echo '<br />';
						echo '<br />';
						echo '<br />';
						echo '<div class="container">';

							echo '<div class="row">';
						
								echo '<div class="col-xs-12 col-sm-6">';

									echo '<br />';
									echo '<a href="' . get_bloginfo('url'). '" class="btn btn-primary">Inicio</a>';

								echo '</div>';
							
								echo '<div class="col-xs-12 col-sm-6">';

									echo '<div class="row">';

										echo '<div class="col-xs-12 col-sm-7">';
											
											echo get_search_form(false);

										echo '</div>';

									echo '</div>';

								echo '</div>';

							echo '</div>';

						echo '</div>';

					}
				?>