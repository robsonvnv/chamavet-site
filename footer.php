				
				<div class="clear"></div>

			</div>
			<footer>

				<div class="container">

					<div class="row">

						<div class="col-xs-12">

							<?php

								wp_nav_menu(array(

									'menu'				=>	'theme_footer_menu',
									'theme_location'	=>	'theme_footer_menu',
									'container_id'		=>	'footer-menu',
									'echo'				=>	true

								));
								
							?>

						</div>

					</div>

				</div>
				<?php wp_footer(); ?>

			</footer>

		</div>

	</body>

</html>